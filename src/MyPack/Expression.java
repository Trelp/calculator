package MyPack;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Trelp
 * Date: 10.03.13
 * Time: 14:51
 * To change this template use File | Settings | File Templates.
 */
public class Expression {
    /**
     * Закрытый конструктор класса.
     */
    private Expression() {

    }

    /**
     * Основные математические операции и их приоритеты.
     */
    public static final Map<String, Integer> MATH_OPERATORS;

    static {
        MATH_OPERATORS = new HashMap<String, Integer>();
        MATH_OPERATORS.put("*", 1);
        MATH_OPERATORS.put("/", 1);
        MATH_OPERATORS.put("+", 2);
        MATH_OPERATORS.put("-", 2);
    }

    /**
     * Преобразует алгебраическое выражение из инфиксной записи в обратную польскую нотацию по
     * по алгоритму Эдсгера Дейкстры "Shunting-yard algorithm".
     * @param operations операторы (лево-ассоциированные).
     * @param leftBkt левая скобка.
     * @param rightBkt правая скобка.
     * @return преобразованное выражение.
     */
    public static String shuntingYard(String algebraicExpression, Map<String, Integer> operations, String leftBkt, String rightBkt) {
        if (algebraicExpression == null || algebraicExpression.length() == 0)
            throw new IllegalStateException("Не указано выражение.");
        if (operations == null || operations.isEmpty())
            throw new IllegalStateException("Не указаны операции.");
        // Выходная строка.
        List<String> outputLine = new ArrayList<String>();
        Stack<String> stack = new Stack<String>();
        // Исключение пробелов из выражения.
        algebraicExpression = algebraicExpression.replace(" ", "");
        // Множество символов (операции и скобки).
        Set<String> operationSymbols = new HashSet<String>(operations.keySet());
        operationSymbols.add(leftBkt);
        operationSymbols.add(rightBkt);

        int index = 0;
        boolean findNext = true;

        while(findNext) {
            int nextOperationsIndex = algebraicExpression.length();
            String nextOperation = "";
            // Поиск оператора или скобки.
            for (String operation : operationSymbols) {
                int i = algebraicExpression.indexOf(operation, index);
                if (i >= 0 && i < nextOperationsIndex) {
                    nextOperation = operation;
                    nextOperationsIndex = i;
                }
            }
            // Выход из цикла если оператор не найден.
            if (nextOperationsIndex == algebraicExpression.length()) {
                findNext = false;
            } else {
                // Если перед оператором или скобкой операнд, то добавляем его в выходную строку.
                if (index != nextOperationsIndex) {
                    outputLine.add(algebraicExpression.substring(index, nextOperationsIndex));
                }
                // Обработка открыв. скобки.
                if (nextOperation.equals(leftBkt)) {
                    stack.push(nextOperation);
                }
                // Обработка закрыв. скобки.
                else if (nextOperation.equals(rightBkt)) {
                    while (!stack.peek().equals(leftBkt)) {
                        outputLine.add(stack.pop());
                        if (stack.empty()) {
                            throw new IllegalStateException("В выражении пропущена скобка");
                        }
                    }
                    stack.pop();
                }
                else {
                    while (!stack.empty() && !stack.peek().equals(leftBkt) &&
                            (operations.get(nextOperation) >= operations.get(stack.peek()))) {
                        outputLine.add(stack.pop());
                    }
                    stack.push(nextOperation);
                }
                index = nextOperationsIndex + nextOperation.length();
            }
        }
        if (index != algebraicExpression.length()) {
            outputLine.add(algebraicExpression.substring(index));
        }
        // Преобразование выходной строки
        while (!stack.empty()) {
            outputLine.add(stack.pop());
        }
        StringBuffer resultString = new StringBuffer();
        if (outputLine.isEmpty()) {
            resultString.append(outputLine.remove(0));
        }
        while (!outputLine.isEmpty()) {
            resultString.append(" ").append(outputLine.remove(0));
        }
        return resultString.toString();
    }
    public static String shuntingYard(String algebraicExpression, Map<String, Integer> operations) {
        return shuntingYard(algebraicExpression, operations, "(", ")");
    }
    public static BigDecimal calcExpression(String expression) {
        String rpn = shuntingYard(expression, MATH_OPERATORS);
        StringTokenizer tokenizer = new StringTokenizer(rpn, " ");
        Stack<BigDecimal> stack  = new Stack<BigDecimal>();
        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            if (!MATH_OPERATORS.keySet().contains(token)) {
                stack.push(new BigDecimal(token));
            } else {
                BigDecimal op2 = stack.pop();
                BigDecimal op1 = stack.empty() ? BigDecimal.ZERO : stack.pop();
                if (token.equals("+")) {
                    stack.push(op1.add(op2));
                } else if (token.equals("-")) {
                    stack.push(op1.subtract(op2));
                } else if (token.equals("*")) {
                    stack.push(op1.multiply(op2));
                } else if (token.equals("/")) {
                    try {
                        stack.push(op1.divide(op2, 5, BigDecimal.ROUND_HALF_UP));
                    }
                    catch (ArithmeticException ex) {
                        System.out.println("Деление на ноль невозможно.");
                        System.exit(1);
                    }
                }
            }
        }
        return stack.pop();
    }
}
