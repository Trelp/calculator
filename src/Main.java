import MyPack.Expression;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner str = new Scanner(System.in);
        String expression = str.nextLine();
        System.out.println("Инфиксная запись: " + expression);
        String rpn = Expression.shuntingYard(expression, Expression.MATH_OPERATORS);
        System.out.println("Обратная польская нотация: " + rpn);
        System.out.println("Ответ: " + Expression.calcExpression(expression));
    }
}
